function totalList(arr) {
    return `<ul>${arr.map(elem => `<li>${elem}</li>`).join('')}</ul>`;
}

function createElem(elem) {
    const innerElem = document.createElement('div');
    innerElem.innerHTML = elem;
    return innerElem;
}

function addElem(inputData) {
    let s = totalList(inputData);
    let divGen = createElem(s);
    window.addEventListener('load', () => {
        document.body.prepend(divGen);
    });
}

let array = ['1', 'world', '3', 'Kharkiv', 'Odessa', 'user', 'Tel-Aviv'];

addElem(array);